const map = L.map('mapid').setView([13.7026557, -89.2143489], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
}).addTo(map);

// L.marker([13.7026557, -89.2143489]).addTo(map);

$.ajax({
	dataType: "json",
	url: "api/bicicletas",
	success: (res) => {
		res.data.forEach(el => {
			L.marker(el.ubicacion, { title: el.id }).addTo(map);
		});
	}
});	