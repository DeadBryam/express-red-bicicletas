let entity = require('../../models/BicicletaDS');

exports.bicicleta_list = (req, res) => {
  res.status(200).json({
    data: entity.findAll
  });
}

exports.bicicleta_create = (req, res) => {
  let bicicleta = new entity(req.body.id, req.body.color, req.body.modelo);
  bicicleta.ubicacion = [req.body.lat, req.body.long];
  entity.add(bicicleta);

  res.status(201).json({
    data: bicicleta
  });
}

exports.bicicleta_delete = (req, res) => {
  entity.removeById(req.params.id);

  res.status(204).send();
}

exports.bicicleta_update = (req, res) => {
  let bicicleta = entity.findById(req.params.id);
  bicicleta.id = req.body.id;
  bicicleta.color = req.body.color;
  bicicleta.modelo = req.body.modelo;
  bicicleta.ubicacion = [req.body.lat, req.body.long]

  res.status(200).json({
    data: bicicleta
  });
}