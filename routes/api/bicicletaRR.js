let express = require('express');
let router = express.Router();
let bicicletaController = require('../../controllers/api/BicicletaRest');

router.get('/', bicicletaController.bicicleta_list);
router.post('/create', bicicletaController.bicicleta_create);
router.delete('/:id/delete', bicicletaController.bicicleta_delete);
router.put('/:id/update', bicicletaController.bicicleta_update);

module.exports = router;