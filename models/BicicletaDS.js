const Bicicleta = function (id, color, modelo, ubicacion) {
  this.id = id;
  this.color = color;
  this.modelo = modelo;
  this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function () {
  return `id: ${this.id} | color: ${this.color} `
}

Bicicleta.findAll = [];
Bicicleta.add = (bici) => {
  Bicicleta.findAll.push(bici);
}

Bicicleta.findById = (id) => {
  let bici = Bicicleta.findAll.find(x => x.id == id);
  if (bici)
    return bici;
  else
    throw new Error(`No existe una bicicleta con el id ${id}`);
}

Bicicleta.removeById = (id) => {

  for (let i = 0; i < Bicicleta.findAll.length; i++) {
    if (Bicicleta.findAll[i].id == id) {
      Bicicleta.findAll.splice(i, 1);
      break;
    }
  }
}


let a = new Bicicleta(17564, 'Verde', 'Urbana', [13.7012485, -89.213512]);
let b = new Bicicleta(67342, 'Azul', 'Urbana', [13.7058557,-89.2131902]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta; 